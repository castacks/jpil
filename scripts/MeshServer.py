###
# Server to receive point cloud from HoloLens
# 
# Developed by Manash Pratim Das
# mpdmanash@iitkgp.ac.in
###

import socket
import sys, os

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to the port
server_address = ('0.0.0.0', 444) # //192.168.43.199 169.254.80.80
print >> sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
# Listen for incoming connections
sock.listen(1)

# Wait for a connection
while True:
	print >>sys.stderr, 'waiting for a connection'
	
	myfile = open("HololensPC.pcd", "a+")
	connection, client_address = sock.accept()
	try:
	    print >>sys.stderr, 'connection from', client_address

	    # Receive the data in small chunks and retransmit it
	    while True:
	        data = connection.recv(4096)
	        #print >>sys.stderr, 'received "%s"' % data
	        if data:
	            #print data
	            if "Done" in data:
	            	data.replace("Done", "")
	            	myfile.write(data)
	            	myfile.close()
	                
	                #Send ping to register code
	                # create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
	                client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	                client.connect(('0.0.0.0', 8888))
	                client.send('g')
	                response = client.recv(8)
	                client.close()
	                connection.send("RT")
	                print "sending RT"
	                try:
	                	os.remove("HololensPC.pcd")
	                except OSError:
	                	pass
	                break
	            else:
	            	myfile.write(data)
	        else:
	            print >>sys.stderr, 'no more data from', client_address            
	            break

	finally:
	    # Clean up the connection
	    connection.close()
