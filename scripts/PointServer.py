###
# Multi-threaded server to facilitate communication and transformation of
# 3D points between HoloLens and Laser Scanned prior point cloud of bridge,
# in their respective coordinate frames
# 
# Developed by Manash Pratim Das
# mpdmanash@iitkgp.ac.in
###
import sys, os
import numpy as np
import thread
import socket

current_connection = None
current_connectionC = None

def server(sockS):
	global current_connection, current_connectionC
	while True:
		print sys.stderr, 'waiting for a connection'
		current_connection, client_address = sockS.accept()
		try:
			print sys.stderr, 'connection from', client_address
			while True:
				data = current_connection.recv(1024)
				if data:
					pass
				else:
					break
				# data = current_connection.recv(1024)
				# if data:
				# 	parts = data.split(':')
				# 	if(len(parts) == 4):
				# 		hlpt = np.matrix( [ [float(parts[0])], [float(parts[1])], [float(parts[2])], [1.0] ] )

				# 		rtf = open("RT.txt", "r")
				# 		line = rtf.read()
				# 		parts = line.split(':')
				# 		trans = np.matrix([ [float(parts[0]), float(parts[1]), float(parts[2]), float(parts[3])], 
				# 			               [float(parts[4]), float(parts[5]), float(parts[6]), float(parts[7])],
				# 			               [float(parts[8]), float(parts[9]), float(parts[10]), float(parts[11])],
				# 			               [float(parts[12]), float(parts[13]), float(parts[14]), float(parts[15])] ])
				# 		print (trans)
				# 		rtf.close()

				# 		pcpt = trans.dot(hlpt)
				# 		new_data = (str(pcpt.item((0,0))/pcpt.item((3,0))) + ':'
				# 		         + str(pcpt.item((1,0))/pcpt.item((3,0))) + ':' 
				# 		         + str(pcpt.item((2,0))/pcpt.item((3,0))) + ':\n' );
				# 		sockC.send(new_data)
		except:
			print "Error in server"

def server2(sockS2):
	global current_connectionC
	while True:
		print sys.stderr, 'waiting for a connection'
		connection, client_address = sockS2.accept()
		try:
			print sys.stderr, 'connection from', client_address
			while True:
				data = connection.recv(1024)
				if data:
					parts = data.split(':')
					if(len(parts) == 4):
						hlpt = np.matrix( [ [float(parts[0])], [float(parts[1])], [float(parts[2])], [1.0] ] )

						rtf = open("RT.txt", "r")
						line = rtf.read()
						parts = line.split(':')
						trans = np.matrix([ [float(parts[0]), float(parts[1]), float(parts[2]), float(parts[3])], 
							               [float(parts[4]), float(parts[5]), float(parts[6]), float(parts[7])],
							               [float(parts[8]), float(parts[9]), float(parts[10]), float(parts[11])],
							               [float(parts[12]), float(parts[13]), float(parts[14]), float(parts[15])] ])
						print (trans)
						rtf.close()

						pcpt = trans.dot(hlpt)
						new_data = (str(pcpt.item((0,0))/pcpt.item((3,0))) + ':'
						         + str(pcpt.item((1,0))/pcpt.item((3,0))) + ':' 
						         + str(pcpt.item((2,0))/pcpt.item((3,0))) + ':\n' );
						current_connectionC.send(new_data)
				else:
					print "Client left"
					break
		except:
			print "Error in server2"


def serverC(sockC):
	global current_connection, current_connectionC
	print current_connection
	while True:
		print sys.stderr, 'waiting for a connection'
		current_connectionC, client_address = sockC.accept()
		try:
			print sys.stderr, 'connection from', client_address
			while True:
				data = current_connectionC.recv(1024)
				if data:
					parts = data.split(':')
					if(len(parts) == 4):
						pcpt = np.matrix( [ [float(parts[0])], [float(parts[1])], [float(parts[2])], [1.0] ] )
						rtf = open("RT.txt", "r")
						line = rtf.read()
						parts = line.split(':')
						trans = np.matrix([ [float(parts[0]), float(parts[1]), float(parts[2]), float(parts[3])], 
							               [float(parts[4]), float(parts[5]), float(parts[6]), float(parts[7])],
							               [float(parts[8]), float(parts[9]), float(parts[10]), float(parts[11])],
							               [float(parts[12]), float(parts[13]), float(parts[14]), float(parts[15])] ])
						print (trans)
						rtf.close()
						
						trans_i = np.linalg.inv(trans)
						hlpt = trans_i.dot(pcpt)
						new_data = (str(hlpt.item((0,0))/hlpt.item((3,0))) + ':' 
							       + str(hlpt.item((1,0))/hlpt.item((3,0))) + ':' 
							       + str(hlpt.item((2,0))/hlpt.item((3,0))) + ':\n' );
						current_connection.send(new_data)
				else:
					print "Client left"
					break
		except:
			print "Error in serverC"



def main():
	global current_connection, current_connectionC

	sockS = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_addressS = ('0.0.0.0', 445)
	print >> sys.stderr, 'starting up on %s port %s' % server_addressS
	sockS.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sockS.bind(server_addressS)
	sockS.listen(1)

	sockS2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_addressS2 = ('0.0.0.0', 446)
	print >> sys.stderr, 'starting up on %s port %s' % server_addressS2
	sockS2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sockS2.bind(server_addressS2)
	sockS2.listen(1)

	sockC = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_addressC = ('0.0.0.0', 8889)
	print >> sys.stderr, 'starting up on %s port %s' % server_addressC
	sockC.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sockC.bind(server_addressC)
	sockC.listen(1)

	thread.start_new_thread( server ,(sockS,) )
	thread.start_new_thread( server2 ,(sockS2,) )
	thread.start_new_thread( serverC ,(sockC, ) )
	# try:
	# 	thread.start_new_thread( server ,(sockS, sockC) )
	# 	thread.start_new_thread( client ,(sockC, ) )
	# except:
	# 	print "Error: unable to start thread"

	var = raw_input("press to exit\n")
	current_connection.close()
	sockC.close()
	sockS.shutdown(socket.SHUT_RDWR)
	os._exit

main() 