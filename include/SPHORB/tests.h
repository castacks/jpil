#ifndef _TESTS_H
#define _TESTS_H

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>

using namespace std;
using namespace cv;



void detectHarrisCorners(Mat &in_img, int bSize, int kSize, double K, Mat &out_img, vector<KeyPoint> &out_kps);
void computeSIFTdescriptors(Mat &img_1, Mat &img_2,
	                        vector<KeyPoint> &kp_1,
	                        vector<KeyPoint> &kp_2,
	                        Mat &desc_1, Mat &desc_2);
void matchDescriptors(Mat &desc_1, Mat &desc_2,
	                  vector<DMatch> &matches);
#endif