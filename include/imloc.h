#ifndef _IMLOC_H
#define _IMLOC_H

#include <iostream>
#include <fstream>
#include <vector>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <opencv2/opencv.hpp>
#include <boost/graph/graph_concepts.hpp>

#include "SPHORB.h"
#include "AKAZE.h"
#include "json/json.h"

#include <stdlib.h> 
#include <time.h>
#include <ceres/ceres.h>
#include <ceres/rotation.h>

#include <omp.h>

struct ba_point{
  float x;
  float y;
  float z;
  float ba;
  uint8_t r;
  uint8_t g;
  uint8_t b;
  float rho;
};



class ImLoc{
public:
  
  /*Generate Image Projection of PC with given camera pose.*/
  ImLoc(int height);
  ~ImLoc();
  void GenProjectionImg(pcl::PointCloud<pcl::PointXYZRGB>::Ptr in_cloud, int width, int height,
      double *camera_pos, std::vector<std::vector<ba_point> >* & out_ba_points);  
  void DetectAKAZEfeatures(cv::Mat & in_img, std::vector<cv::KeyPoint> & out_kps);  
  void ComputeAKAZEdescriptors(cv::Mat & in_img, std::vector<cv::KeyPoint> & in_kps, int desc_id, cv::Mat & out_desc);
  void ComputeSPHORBdescriptors(int num_desc, cv::Mat &in_img, std::vector<cv::KeyPoint> & out_kps, cv::Mat & out_desc);
  void MatchAKAZEfeatures(cv::Mat in_desc_1, cv::Mat in_desc_2, int num_nns, std::vector<cv::DMatch> & out_matches);
  void MatchSPHORBfeatures(cv::Mat in_desc_1, cv::Mat in_desc_2, int num_nns, std::vector<cv::DMatch> & out_matches);
  void ScaleKeypoints(std::vector<cv::KeyPoint> & in_kps, std::vector<cv::KeyPoint> & out_kps, double scale_factor);
  void WriteBAimage(std::string fileName, std::vector<std::vector<ba_point> >* & in_ba_points, double *camera_pos);
  void ReadBAimage(std::string fileName, std::vector<std::vector<ba_point> >*& out_ba_points, double* & out_camera_pos);
  void ConvertToImage(std::vector<std::vector<ba_point> >*& in_ba_points, cv::Mat & out_image);
  bool IsWithinImageBounds(int x, int y, int width, int height);
  void Get3dPoints(std::vector<cv::KeyPoint> & in_kps, std::vector<std::vector<ba_point> >*& in_ba_points,
                   std::vector<cv::Vec4f> & out_kps3d, float std_threshold = 1.0);
  std::string type2str(int type);
  void WriteDesc3DPoints(string fileName, std::vector<cv::KeyPoint> & in_kps, 
       std::vector<cv::Vec4f> & in_kps3d, cv::Mat & in_desc);
  void ReadDesc3DPoints(string fileName, std::vector<cv::KeyPoint> & out_kps, 
      std::vector<cv::Vec4f> & out_kps3d, cv::Mat & out_desc);
  void Image2Sphere(int x, int y, double & sx, double & sy, double & sz);
  void Sphere2Image(double sx, double sy, double sz, double& x, double& y);
  double World2Sphere(double x, double y, double z, double * camera, double & sx, double & sy, double & sz);
  void RunOptimizer(int num_pairs, double s_obs[][3], double world_obs[][3], double * orientation, double angle_bound, double pos_bound, double * out_trans, bool use_initial=false);
  void FindInliers(std::vector<cv::DMatch> & in_matches, std::vector<cv::Vec4f> & in_kps3d, int height,
       std::vector<cv::KeyPoint> & in_kps, double * solved_camera, double threshold,
       int & out_num_inliers, std::vector<int> & out_in_ids);
  void SolveSnPRansac(std::vector<cv::DMatch> & in_matches, std::vector<cv::Vec4f> & in_kps3d, std::vector<cv::KeyPoint> & in_kps,
                int num_ransac, double threshold, int height, double * orientation, double angle_bound, double pos_bound, double * final_trans, std::vector<int> & out_inliers, double * GT=nullptr);
  void DrawMatches(const cv::Mat& img1, const std::vector<cv::KeyPoint>& keypoints1, 
       const cv::Mat& img2, const std::vector<cv::KeyPoint>& keypoints2,
       const std::vector<cv::DMatch>& matches1to2, cv::Mat& outImg, const cv::Scalar& matchColor, const cv::Scalar& singlePointColor,
       const std::vector<char>& matchesMask, int flags , bool vertical);
  void DrawMultiMatches(std::vector<cv::DMatch> & in_matches, std::vector<cv::KeyPoint> & q_kps, std::vector<cv::KeyPoint> & db_kps,
            std::vector<int> & in_end_ids, std::vector<cv::Mat> & in_db_images, cv::Mat & in_q_image,
            std::vector<int> & inliers, bool draw_inliers);
  void ComputeVariance(vector< Vec3f >& points, float &out_varx, float &out_vary, float &out_varz);
  void ComputeDeviationHeatmap(std::vector<std::vector<ba_point> >*& in_ba_points, int radius, float clip_std, cv::Mat & out_heatmap);
  
private:
  void _drawKeypoint( cv::Mat& img, const cv::KeyPoint& p, const cv::Scalar& color, int flags );
  void _prepareImgAndDrawKeypoints( const cv::Mat& img1, const std::vector<cv::KeyPoint>& keypoints1,
                                     const cv::Mat& img2, const std::vector<cv::KeyPoint>& keypoints2,
                                     cv::Mat& outImg, cv::Mat& outImg1, cv::Mat& outImg2,
                                     const cv::Scalar& singlePointColor, int flags );
  void _drawMatch( cv::Mat& outImg, cv::Mat& outImg1, cv::Mat& outImg2, const cv::KeyPoint& kp1,
         const cv::KeyPoint& kp2, const cv::Scalar& matchColor, int flags );
  void _ratioTest(const std::vector<std::vector<cv::DMatch> >& knMatches, float maxRatio, std::vector<cv::DMatch>& goodMatches);
  
  int _image_height;
  int _image_center;
  double _scale_factor;
};



#endif