/**
 * This is an implementation of a modified Binary Shape Context descriptor based Point Cloud Registration
 * Find more about Binary Shape Context descriptor at 
 * https://www.researchgate.net/publication/318445009_A_novel_binary_shape_context_for_3D_local_surface_description
 * 
 * Author: Manash Pratim Das
 * Email: mpdmanash@iitkgp.ac.in
 */

#ifndef _tBSC_H
#define _tBSC_H

#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <Eigen/Core>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/transforms.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/correspondence.h>
#include <pcl/common/common.h>
#include <pcl/search/kdtree.h>

#include <opencv2/opencv.hpp>
#include <boost/graph/graph_concepts.hpp>

#include <stdlib.h> 
#include "json/json.h"
#include <time.h>
#include <omp.h>

#define tbscDEBUG true

namespace tbsc{
  struct Vector3f{
    float x; float y; float z;
    Vector3f(float a, float b, float c) {
      x = a;
      y = b;
      z = c;
    }
    Vector3f(): x(0), y(0), z(0) {}
  };

  struct Vector2f{
    float x; float y;
    Vector2f(float a, float b) {
      x = a;
      y = b;
    }
    Vector2f(): x(0), y(0){}
  };
  
  struct DMatch{
    int queryIdx; int trainIdx; int distance;
    DMatch(int q, int t, int d) {
      queryIdx = q;
      trainIdx = t;
      distance = d;
    }
    DMatch(): queryIdx(0), trainIdx(0), distance(0){}
  };
};


class tBSC{
public:
  tBSC(int num_pairs=128, int s=7);
  void Downsample(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, float leaf_size, pcl::PointCloud<pcl::PointXYZ>::Ptr out_cloud);
  void DetectKeypoints(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr kdtree,
           float search_radius, float threshold, std::vector<int> & out_kps, std::vector<tbsc::Vector3f> & out_X);
  void DrawKeypoints(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, std::vector<int> & kps, pcl::PointCloud<pcl::PointXYZRGB>::Ptr out_cloud);
  void ComputeDescriptors(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr kdtree,
        std::vector<int> & in_kps, std::vector<tbsc::Vector3f> & in_X, float radius,
        std::vector<std::vector<bool> > & out_descs, int h=4);
  void ComputeSingleDescriptor(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr kdtree,
             tbsc::Vector3f & in_X, float radius, pcl::PointXYZ centerPt,
             std::vector<bool> & out_descs, int h=4);
  void CompareMatchesWithGT(std::vector<cv::DMatch> & matches, pcl::PointCloud<pcl::PointXYZ>::Ptr pc1, pcl::PointCloud<pcl::PointXYZ>::Ptr pc2,
          std::vector<int> & kps_1, std::vector<int> & kps_2, std::vector<cv::DMatch> & inliers);
  void InvertXaxes(std::vector<tbsc::Vector3f> & in_X, std::vector<tbsc::Vector3f> & out_X);
  void MatchDescriptors(std::vector<std::vector<bool> > & desc_q, std::vector<std::vector<bool> > & desc_q2,
      std::vector<std::vector<bool> > & desc_t, float threshold, std::vector<tbsc::DMatch> & out_matches);
  void ConvertToCVdescriptors(std::vector<std::vector<bool> > & descs, cv::Mat & out_desc);
  void MatchCVDescriptors(cv::Mat & desc_q, cv::Mat & desc_t, std::vector<cv::DMatch> & out_matches, double threshold_ratio);
  void FindTransformation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_t,
        std::vector<int> & kps_q, std::vector<int> & kps_t,
        std::vector<cv::DMatch> & inliers, Eigen::Matrix4f & out_trans);
  void DrawRegistration(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_q_al, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_t,
      pcl::PointCloud<pcl::PointXYZRGB>::Ptr out_cloud);
  void GeometricConsistantInliers(std::vector<cv::DMatch> & matches, pcl::PointCloud<pcl::PointXYZ>::Ptr pc1, pcl::PointCloud<pcl::PointXYZ>::Ptr pc2,
                std::vector<int> & kps_1, std::vector<int> & kps_2, float threshold, std::vector<cv::DMatch> & inliers, std::vector<cv::DMatch> & outliers, double * GT=nullptr);
  void RansacInliers(std::vector<cv::DMatch> & matches, pcl::PointCloud<pcl::PointXYZ>::Ptr pc1, pcl::PointCloud<pcl::PointXYZ>::Ptr pc2,
               std::vector<int> & kps_1, std::vector<int> & kps_2, float threshold, std::vector<cv::DMatch> & inliers, std::vector<cv::DMatch> & outliers, double * GT=nullptr);
  void WriteKpX(std::vector<int> & kps, std::vector<tbsc::Vector3f> & X, std::string filename);
  void ReadKpX(std::vector<int> & kps, std::vector<tbsc::Vector3f> & X, std::string filename);
  void WriteDesc(cv::Mat & desc, std::string filename);
  void ReadDesc(cv::Mat & desc, std::string filename);
  float ComputeHausdorff(pcl::PointCloud<pcl::PointXYZ> & cloud_A, pcl::PointCloud<pcl::PointXYZ> & cloud_B);
  void ComputeVariance(std::vector< cv::Vec3f >& points, float &out_varx, float &out_vary, float &out_varz);

private:
  int m_num_pairs;
  int m_s;
  std::vector<std::pair<int, int> > m_pairs;
  void _ComputeEigenVectorsValues(Eigen::MatrixX3d & traindata, Eigen::MatrixXd & evecs, Eigen::VectorXd & evels);
  void _TransformCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, tbsc::Vector3f & in_X, pcl::PointCloud<pcl::PointXYZ>::Ptr out_cloud);
  double _GetWeightedDensity();
  double _GetWeightedDistance(pcl::PointXYZ pt, int plane);
  int _HammingDistance(const std::vector<bool> & desc1, const std::vector<bool> & desc2);
  void _FindInliers(std::vector<cv::DMatch> & matches, pcl::PointCloud<pcl::PointXYZ>::Ptr pc1, pcl::PointCloud<pcl::PointXYZ>::Ptr pc2,
        std::vector<int> & kps_1, std::vector<int> & kps_2, Eigen::Matrix4f & trans, float threshold, std::vector<cv::DMatch> & inliers,
        std::vector<cv::DMatch> & outliers);
  float _Distance(pcl::PointXYZ pt1, pcl::PointXYZ pt2);
  void _estimateTranslation(pcl::PointCloud< pcl::PointXYZ> & cloud_q, 
                            pcl::PointCloud< pcl::PointXYZ> & cloud_t,
                            pcl::Correspondences & correspondences,
                            Eigen::Matrix4f& out_trans);
};

#endif