
# Joint Point Cloud and Image Based Localization For Efficient Inspection in Mixed Reality
Point cloud registration and Maximum Likelihood Camera Pose Estimation to localize a Mixed-Reality Headset (Microsoft HoloLens) w.r.t. a Reference Point Cloud.   
The Air Lab, Robotics Institute, Carnegie Mellon University.

[**Check out our YouTube-Video, showing JPIL in Action**](https://www.youtube.com/watch?v=1EEftDU7BIw)  
[![Joint Point Cloud and Image Based Localization For Efficient Inspection in Mixed Reality](https://img.youtube.com/vi/1EEftDU7BIw/0.jpg)](https://www.youtube.com/watch?v=1EEftDU7BIw)

## Publication
If you use this code in an academic context, please cite the following [IROS 2018 paper](https://arxiv.org/pdf/1811.02563.pdf).

Manash Pratim Das, Zhen Dong, Sebastian Scherer: **Joint Point Cloud and Image Based Localization For Efficient Inspection in Mixed Reality**, IEEE/RSJ Int. Conf. Intell. Robot. Syst. (IROS), 2018.

```
@InProceedings{Das2018
  author = {Das, Manash Pratim and Dong, Zhen and Scherer, Sebastian},
  title = {Joint Point Cloud and Image Based Localization For Efficient Inspection in Mixed Reality},
  booktitle = {IEEE/RSJ Int. Conf. Intell. Robot. Syst. (IROS)},
  year = {2018}
}
```

## Installation, Documentation, And Usage
Dependencies: [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page), [PCL](https://github.com/PointCloudLibrary/pcl), [OpenCV](https://github.com/opencv/opencv), [Ceres-Solver](http://ceres-solver.org/),  OpenMP, c++ Threads with c++0x compiler support.  
`$ cd jpil`  
`$ mkdir build`  
`$ cd build`  
`$ cmake ..`  
`$ make`  

### Documentation
- `run_mcr`: Performs Multiple-Candidate Point Cloud Registration
	- Inputs: 
		- Path to reference point cloud: For eg: `Data/edm_template_downsampled_clipped.pcd`
		- Path to HoloLens point cloud: For eg: `Data/HoloLens_PCD/eneManual_2.pcd`
		- Path to Spherical Image from Headset: For eg: `Data/spherical_images/H5.JPG`
		- Ground truth position of the Headset wr.t the origin of Headset point cloud (Only for evaluation): Set it to `0 0 0` if not known.
		- Orientation of Headset in ENE frame from Headset IMU and Magnetometer (Quaternion): `w x y z`
		- Path to Dense RGB reference point cloud (Used to generate synthetic projection images for the candidates): Dowload it from [here](https://cmu.box.com/s/dmjvutj55e7b4plc47hkjjwpmojiar0g).
	- Outputs:
		- Transformations for each PC registration candidates exported to `RTX.txt`
		- Point clouds showing keypoints selected from both reference (`keypoints_t.pcd`) and HoloLens PCs (`keypoints_q.pcd`).
		- HL pointcloud added to reference PC for each PC registration candidates. (named as `registeredX.pcd`)
		- Finally, it exports virtual projection images in two formats (`jpg` for visualization and `json` for usage in JPIL. The `json` version contains additional information that cannot be stored in `jpg`).
	- Running in cached mode: For point cloud registration, keypoints are detected and tBSC descriptors are computed for both reference PC and HoloLens PC. Since the reference point cloud does not change, we can save computation time by saving the keypoints and descriptors for the same. To enable caching, please set `use_cached = true` in `run_mcr.cpp`.
	
- `run_cpe`: Performs Maximum Likelihood Estimation to estimate the pose of the Spherical Camera. `run_mcr` gave multiple possible candidates. So to find the best candidate, we now match our real Spherical Image with virtual projection images for each candidates exported by `run_mcr`. This implementation just shows matching for one candidate, for demonstration purposes.
	- Inputs:
		- Path to the candidate virtual `json` image: For eg: `Data/cached/ba_image2.json`
		- Path to real `jpg` image from Spherical camera: For eg: `Data/cached/H5_R.jpg`
	- Outputs:
		- Localization error in (m) if GT is provided.
		- Final image showing 2D-2D inlier image matchs between real and virtual images (`matches_stddev_X.jpg`) 

### Demo Run
Example dataset is provided in the `Data` directory and a large RGB reference PC can be downloaded from [this link](https://cmu.box.com/s/dmjvutj55e7b4plc47hkjjwpmojiar0g).  

- `$ cd build`  
- `$ ./run_mcr ../Data/edm_template_downsampled_clipped.pcd ../Data/HoloLens_PCD/eneManual_2.pcd ../Data/spherical_images/H5.JPG -1.62169 4.83154 0.032307 0.817653 -0.25642 -0.23564 0.458438 <path-to-dense-RGB-PC>`  
- `$ ./run_cpe ../Data/cached/ba_image2.json ../Data/cached/H5_R.jpg`  



Contact: Manash Pratim Das (mpdmansh @ gmail . com)

