#include "tBSC.h"
#include "imloc.h"
#include <sstream>

std::vector<std::vector<ba_point> >* ba_points;

bool use_cached = false;
bool export_ba_images = true;

int main(int argc, char *argv[]){
  /*
   * Inputs:
   * 1) EDM_Template_Downsampled
   * 2) ENE_Query point cloud
   * 3) Spherical Image (I)
   * 4) Position (P) in ENE_Query frame
   * 5) Orientation (O) in ENE
   * 6) EDM_Template_RGB
   * 
   * Steps:
   * 1) Perform multiple candidate registration bw ENE_Query and EDM_Template_Downsampled using tBSC
   * 2) Now for each candidate-
   *    a) Find Position P' in EMD_Template from the estimated RT.
   *    b) Generate BA image (BAI) from EDM_Template_RGB with P' and O.
   *    c) Perform Image Matching between BAI and I.
   *    d) Cache things for faster processing.
   * 3) Find which image matching is best, thus select the final candidate.
   * 
   * Example run: 
   * ./run_aicmr 
   * ../Data/edm_template_downsampled_clipped.pcd 
   * ../Data/HoloLensPCD/eneManual_2.pcd 
   * ../Data/spherical_images/H5.JPG 
   * -1.62169 4.83154 0.032307 0.817653 -0.25642 -0.23564 0.458438 
   * ../Data/edm_template_downsampled_RGB.pcd
   */
  
  // Step 1 ======================================================================
  pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_tu(
      new pcl::PointCloud< pcl::PointXYZ >);
  pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_t(
      new pcl::PointCloud< pcl::PointXYZ >);
  pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_qu(
      new pcl::PointCloud< pcl::PointXYZ >);
  pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q(
      new pcl::PointCloud< pcl::PointXYZ >);
  pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q_aligned(
      new pcl::PointCloud< pcl::PointXYZ >);
  pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_q(
      new pcl::PointCloud< pcl::PointXYZRGB >);
  pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_t(
      new pcl::PointCloud< pcl::PointXYZRGB >);
  pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_r(
      new pcl::PointCloud< pcl::PointXYZRGB >);
  pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree_t(
      new pcl::KdTreeFLANN< pcl::PointXYZ >);
  pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree_q(
      new pcl::KdTreeFLANN< pcl::PointXYZ >);
  std::vector< std::vector< bool > > desc_t, desc_q;
  cv::Mat cvdesc_t, cvdesc_q;
  std::vector< int > kps_t, kps_q;
  std::vector< tbsc::Vector3f > X_t, X_q;
  if (pcl::io::loadPCDFile< pcl::PointXYZ >(argv[1], *cloud_t) == -1) {
    PCL_ERROR("Couldn't read file test_pcd.pcd \n");
    return (-1);
  }  
  if (pcl::io::loadPCDFile< pcl::PointXYZ >(
    argv[2], *cloud_q) == -1) {
    PCL_ERROR("Couldn't read file test_pcd.pcd \n");
    return (-1);
  }
  std::string spherical_image_name = argv[3];
  Eigen::Vector4f ene_position(std::atof(argv[4]),
                               std::atof(argv[5]),
                               std::atof(argv[6]),
                               1);
  Eigen::Quaternionf orientation(std::atof(argv[7]),
                                 std::atof(argv[8]),
                                 std::atof(argv[9]),
                                 std::atof(argv[10]));  
  std::string edm_template_rgb_name = argv[11];

  //tBSC parameters
  int num_pairs = 200; // Length of the descriptor is proportional to this
  double kp_radius = 0.3;
  double kp_curvature_factor = 0.8;
  double dc_radius = 2.0;
  double ransac_threshold = 0.8;
  double descriptor_matching_cost_threshold = 0.8;
  
  tBSC tbsc(num_pairs);
  kdtree_t->setInputCloud(cloud_t);

  // Gather data (Keypoints and Descriptors of Reference Point Cloud)
  if (use_cached)
  {
    tbsc.ReadKpX(kps_t, X_t, "../Data/cached/kpx_edm_template_downsampled_clipped_3_8_tbsc.json");
    tbsc.ReadDesc(cvdesc_t, "../Data/cached/desc_edm_template_downsampled_clipped_3_8_20_tbsc.json");
  }
  else
  {
    tbsc.DetectKeypoints(cloud_t, kdtree_t, kp_radius, kp_curvature_factor, kps_t, X_t);
    tbsc.WriteKpX(kps_t, X_t, "../Data/cached/kpx_edm_template_downsampled_clipped_3_8_tbsc.json");
    tbsc.DrawKeypoints(cloud_t, kps_t, display_t);
    pcl::io::savePCDFileASCII("keypoints_t.pcd", *display_t); // Output a PC with colored keypoints

    tbsc.ComputeDescriptors(cloud_t, kdtree_t, kps_t, X_t, dc_radius, desc_t);
    tbsc.ConvertToCVdescriptors(desc_t, cvdesc_t);
    tbsc.WriteDesc(cvdesc_t, "../Data/cached/desc_edm_template_downsampled_clipped_3_8_20_tbsc.json");
  }
  
  
  // Prepare the HoloLens Point Cloud. Detect keypoints and Compute Descriptors
  //tbsc.Downsample(cloud_qu, 0.3f, cloud_q);  // Incase we need to down sample it. Mind the variable names though.
  kdtree_q->setInputCloud(cloud_q);
  tbsc.DetectKeypoints(cloud_q, kdtree_q, kp_radius, kp_curvature_factor, kps_q, X_q);
  tbsc.DrawKeypoints(cloud_q, kps_q, display_q);
  pcl::io::savePCDFileASCII("keypoints_q.pcd", *display_q);
  tbsc.ComputeDescriptors(cloud_q, kdtree_q, kps_q, X_q, dc_radius, desc_q);
  tbsc.ConvertToCVdescriptors(desc_q, cvdesc_q);
  
  // Match the 3D point cloud descriptors between HoloLens PC and reference PC
  std::vector< cv::DMatch > matches, outliers;
  tbsc.MatchCVDescriptors(cvdesc_q, cvdesc_t, matches, descriptor_matching_cost_threshold);
  std::vector< cv::DMatch > gtinliers1, rcinliers, gtinliers2;
  
  pcl::PointXYZ min_pt, max_pt;
  pcl::getMinMax3D<pcl::PointXYZ> (*cloud_q, min_pt, max_pt);
  pcl::PointXYZ total_center_q((max_pt.x-min_pt.x)/2.0 + min_pt.x , (max_pt.y-min_pt.y)/2.0 + min_pt.y, (max_pt.z-min_pt.z)/2.0 + min_pt.z);
  float total_radius = std::sqrt((total_center_q.x-min_pt.x)*(total_center_q.x-min_pt.x) +
                        (total_center_q.y-min_pt.y)*(total_center_q.y-min_pt.y) +
                        (total_center_q.z-min_pt.z)*(total_center_q.z-min_pt.z)) ;
  std::cout << "Total radius: " << total_radius << std::endl;

  ImLoc imloc1(640);
  int height = 640; int width = 2 * height;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr edm_template_rgb_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  if(export_ba_images)
  {
    std::cout << "Loading RGB Point Cloud\n";
    if (pcl::io::loadPCDFile<pcl::PointXYZRGB> (edm_template_rgb_name, *edm_template_rgb_cloud) == -1){
      PCL_ERROR ("Couldn't read file test_pcd.pcd \n");
      return (-1);
    }
  }

  bool do_match = true;
  int i = 0;
  while (do_match){
    std::cout << "Running matching for iteration " << i << std::endl;
    std::stringstream ss;
    tbsc.GeometricConsistantInliers(matches, cloud_q, cloud_t, kps_q, kps_t, ransac_threshold, rcinliers, outliers);
    if(rcinliers.size() < 5) do_match = false;
    Eigen::Matrix4f trans;
    tbsc.FindTransformation(cloud_q, cloud_t, kps_q, kps_t, rcinliers, trans);
    std::cout << "PC registration transform" << std::endl << trans <<  std::endl;
    //Find Position P' in EMD_Template from the estimated RT.
    Eigen::Vector4f edm_position = trans * ene_position;
    //std::cout << "EDM_position: " << edm_position[0] << "," << edm_position[1] << "," << edm_position[2] << std::endl;
    Eigen::Vector4f gt_edm_position(7.7373,1.7123,0.5637,1); // Hardcoded ground truth value for evaluation
    double error = std::sqrt( (edm_position[0]-gt_edm_position[0])*(edm_position[0]-gt_edm_position[0]) +
                              (edm_position[1]-gt_edm_position[1])*(edm_position[1]-gt_edm_position[1]) +
                              (edm_position[2]-gt_edm_position[2])*(edm_position[2]-gt_edm_position[2])  );
    std::cout << "EDM_error: " << error << std::endl;
    
    if(export_ba_images)
    {
      //Generate BA image (BAI) from EDM_Template_RGB with P' and O.
      cv::Mat ba_img;
      double camera[7] = {orientation.w(),orientation.x(),orientation.y(),orientation.z(),
                          edm_position[0],edm_position[1],edm_position[2]};
      imloc1.GenProjectionImg(edm_template_rgb_cloud, width, height, camera, ba_points);
      imloc1.ConvertToImage(ba_points, ba_img);
      ss.str(std::string());
      ss << "ba_image" << i << ".jpg";
      imwrite(ss.str(), ba_img);
      ss.str(std::string());
      ss << "ba_image" << i << ".json";
      imloc1.WriteBAimage(ss.str(), ba_points, camera);
    }
    
    //Save transformed point clouds. Each one is a candidate match
    pcl::transformPointCloud(*cloud_q, *cloud_q_aligned, trans);
    std::cout << trans << "\n";
    
    tbsc.DrawRegistration(cloud_q_aligned, cloud_t, display_r);
    ss.str(std::string());
    ss << "registered" << i << ".pcd";
    pcl::io::savePCDFileASCII(ss.str(), *display_r);
    
    std::ofstream myfile;
    ss.str(std::string());
    ss << "RT" << i << ".txt";
    myfile.open(ss.str());
    for (int m = 0; m < 4; m++)
      for (int n = 0; n < 4; n++)
        myfile << trans(m, n) << ":";
    myfile.close();
  
    matches = outliers;
    std::cout << matches.size() << " matches left \n\n\n";
    i++;
  }

  kps_q.clear();
  X_q.clear();
  desc_q.clear();
  cvdesc_q.release();
  return 0;
}