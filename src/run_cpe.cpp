#include "imloc.h"
#include <sstream>
#include <string.h>
using namespace std;
using namespace cv;
vector<vector<ba_point> >* ba_points;

int main(int argc, char *argv[]){
  /*
   * Input: BA image, normal image
   * 1) BAimage
   * 2) Spherical image
   * Output: translation estimate
   */
  ImLoc imloc(640);
  int height = 640; int width = 2 * height;
  double * bacam;
  Mat ba_image, sp_image, sp_image1, match_viz;
  std::vector<cv::KeyPoint> sp_kps, ba_kps;
  cv::Mat ba_desc, sp_desc;
  std::vector<cv::Vec4f> keypoints3d;
  vector<DMatch> all_matches, inliers;
  vector<int> inliers_id;
  double t[7] = {0};
  double angle_bound = 5.0*M_PI/180.0;
  double pos_bound = 100;
  double std_dev_limit = 1;
  
  imloc.ReadBAimage(argv[1], ba_points, bacam);
  double orientation[4] = {bacam[0],bacam[1],bacam[2],bacam[3]};
  double GT[3] = {bacam[4],bacam[5],bacam[6]};
  imloc.ConvertToImage(ba_points, ba_image);
  imloc.ComputeSPHORBdescriptors(10000, ba_image, ba_kps, ba_desc);
  // imloc.DetectAKAZEfeatures(ba_image, ba_kps);
  // imloc.ComputeAKAZEdescriptors(ba_image, ba_kps, 4, ba_desc);
  cout << "Detected " << ba_kps.size() << " SPHORB features ba\n";
  
  //sp_image1 = imread(argv[2]);
  //resize(sp_image1, sp_image, Size(1280,640));
  sp_image = imread(argv[2]);
  
  imloc.ComputeSPHORBdescriptors(10000, sp_image, sp_kps, sp_desc);
  // imloc.DetectAKAZEfeatures(sp_image, sp_kps);
  // imloc.ComputeAKAZEdescriptors(sp_image, sp_kps, 4, sp_desc);
  cout << "Detected " << sp_kps.size() << " SPHORB features sp\n";
  
  imloc.MatchSPHORBfeatures(sp_desc, ba_desc, 2, all_matches);
  
  //for(int iter=0; iter<20; iter++){
    imloc.Get3dPoints(ba_kps, ba_points, keypoints3d, std_dev_limit);
    
    imloc.SolveSnPRansac(all_matches, keypoints3d, sp_kps, 6000, 0.05, height, orientation, angle_bound, pos_bound, t, inliers_id, GT);
    for(int i=0; i<inliers_id.size();i++){
      inliers.push_back(all_matches[inliers_id[i]]);
    }
    
    double error = std::sqrt( (GT[0]-t[4])*(GT[0]-t[4]) +
                              (GT[1]-t[5])*(GT[1]-t[5]) +
                              (GT[2]-t[6])*(GT[2]-t[6]) );
    cout << "Localization error: " << error << "m" << endl;
    
    imloc.DrawMatches(sp_image, sp_kps, ba_image, ba_kps,
                      inliers, match_viz, cv::Scalar::all(-1), cv::Scalar::all(-1),  
                      std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS,true);
    stringstream ss;
    ss << "matches_stddev_" << std_dev_limit*10.0 << ".jpg"; 
    imwrite(ss.str(), match_viz);
    std::cout << ss.str() << std::endl;
    std_dev_limit += 0.1;
  //}
  
  return 0;
}