#include "tests.h"



void detectHarrisCorners(Mat &in_img, int bSize, int kSize,
                         double K, Mat &out_img,
                         vector<KeyPoint> &out_kps){
	Mat img_gray;
	cvtColor(in_img, img_gray, CV_BGR2GRAY);
	cornerHarris(img_gray, out_img, bSize, kSize, K);
	SimpleBlobDetector::Params params;
	params.filterByArea = true;
	params.minArea = 6;
	params.filterByArea = true;
	params.blobColor = 255;
	params.filterByCircularity = false;
	params.filterByConvexity = false;
	params.filterByInertia = false;

	SimpleBlobDetector detector(params);
	imshow("out_img", out_img);
	Mat cvtf;
	out_img.convertTo(cvtf, CV_8UC1);
	imshow("out_img2", cvtf);
	waitKey(0);
	//detector.detect(out_img, out_kps);
}

void computeSIFTdescriptors(Mat &img_1, Mat &img_2,
	                        vector<KeyPoint> &kp_1,
	                        vector<KeyPoint> &kp_2,
	                        Mat &desc_1, Mat &desc_2){
	SiftDescriptorExtractor extractor;
	extractor.compute( img_1, kp_1, desc_1 );
	extractor.compute( img_2, kp_2, desc_2 );
}

void matchDescriptors(Mat &desc_1, Mat &desc_2,
	                  vector<DMatch> &matches){
	if(desc_1.type()!=CV_32F) {
		desc_1.convertTo(desc_1, CV_32F);
	}

	if(desc_2.type()!=CV_32F) {
		desc_2.convertTo(desc_2, CV_32F);
	}

	FlannBasedMatcher matcher;
	std::vector< DMatch > all_matches;
	matcher.match( desc_1, desc_2, all_matches );

    double max_dist = 0; double min_dist = 700;

	//-- Quick calculation of max and min distances between keypoints
	for( int i = 0; i < desc_1.rows; i++ )
	{
		double dist = all_matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	}

	for( int i = 0; i < desc_1.rows; i++ )
	{ 
		if( all_matches[i].distance <= max(2*min_dist, 0.02) )
			matches.push_back( all_matches[i]);
	}
}