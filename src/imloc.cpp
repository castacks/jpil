#include "imloc.h"
#include "../include/imloc.h"

struct SphericalReprojectionError {
  SphericalReprojectionError(double observed_x, double observed_y, double observed_z, double world_x, double world_y, double world_z,
                             double qw, double qx, double qy, double qz)
      : observed_x(observed_x), observed_y(observed_y), observed_z(observed_z), world_x(world_x), world_y(world_y), world_z(world_z),
        qw(qw), qx(qx), qy(qy), qz(qz){}

  template <typename T>
  bool operator()(
      const T* const tx,
      const T* const ty,
      const T* const tz,
      const T* const rx,
      const T* const ry,
      const T* const rz,
      T* residuals) const {
        
    //World2Sphere
    T pw[3] = {T(world_x), T(world_y), T(world_z)};    
    T pc[3]; T pi[3]; T ps[3];
    //const T camera[4] = {T(qw), T(qx), T(qy), T(qz)};
    pw[0] -= tx[0]; pw[1] -= ty[0]; pw[2] -= tz[0];
    //ceres::QuaternionRotatePoint(camera, pw, pc);
    T rot[3] = {rx[0], ry[0], rz[0]};
    ceres::AngleAxisRotatePoint(rot,pw,pc);
    pi[0] = pc[0];
    pi[1] = -pc[2];
    pi[2] = pc[1];
    const T& rho = ceres::sqrt( (pi[0]*pi[0]) +
                                (pi[1]*pi[1]) +
                                (pi[2]*pi[2]) );
    ps[0] = pi[0]/rho;
    ps[1] = pi[1]/rho;
    ps[2] = pi[2]/rho;
    
    residuals[0] = ps[0]-T(observed_x);
    residuals[1] = ps[1]-T(observed_y);
    residuals[2] = ps[2]-T(observed_z);
    //std::cout << double(residuals[0]) << " " << double(residuals[1]) << " " << double(residuals[2]) << " Residuals" << std::endl;
    return true;
  }
private:

  const double observed_x;
  const double observed_y;
  const double observed_z;
  const double world_x;
  const double world_y;
  const double world_z;
  const double qw;
  const double qx;
  const double qy;
  const double qz;
};

ImLoc::ImLoc(int image_height){
  _image_height = image_height;
  _image_center = image_height/2;
  _scale_factor = (double)(image_height)/M_PI;
}

ImLoc::~ImLoc(){}

/*Generate Image Projection of PC with given camera coordinates.*/
void ImLoc::GenProjectionImg(pcl::PointCloud<pcl::PointXYZRGB>::Ptr in_cloud, int width, int height,
           double *camera_pos, std::vector<std::vector<ba_point> >* & out_ba_points){
  if (NULL != out_ba_points) 
  {
      delete out_ba_points;
  }
  out_ba_points = new std::vector<std::vector<ba_point> > (height, std::vector<ba_point>(width));
  double s = height/M_PI;
  double max_distance = 20.0;
  double half_distance = 5.0;
  double alpha = 0.693147181 / half_distance;
  
  
  /*double camera1[6] = {-5*M_PI/180.0, -10*M_PI/180.0, 76*M_PI/180.0, 7.7373,1.7123,0.5637};
  double anotherrot [6] =  {-7*M_PI/180.0,-30*M_PI/180.0,-18*M_PI/180.0,    0,0,0};
  double q[4], q1[4], q2[4];
  ceres::AngleAxisToQuaternion(camera1, q);
  ceres::AngleAxisToQuaternion(anotherrot, q1);
  ceres::QuaternionProduct(q1, q, q2);
  //double r[9];
  //ceres::QuaternionToRotation(q2, r);
  for(int m=0; m<4;m++){
    std::cout<<q2[m] << ",";
  }
  std::cout << std::endl;
    //ceres::AngleAxisRotatePoint(camera_pos, temp_point_w, temp_point_c1);    
    //ceres::AngleAxisRotatePoint(anotherrot, temp_point_c1, temp_point_c);
  */
  
  //double quat[4] = {camera_pos[0],camera_pos[1],camera_pos[2],camera_pos[3]};
  for (int i = 0; i < in_cloud->points.size(); i++){
    /*double temp_point_w[3] = {0.0}; double temp_point_c[3] = {0.0}; double temp_point_c1[3] = {0.0};
    double point_image_frame[3] = {0.0};
    temp_point_w[0] = in_cloud->points[i].x; //Change of axis specific to the PC
    temp_point_w[1] = in_cloud->points[i].y;
    temp_point_w[2] = in_cloud->points[i].z;        
      //temp_point_w[0] -= camera_pos[3]; temp_point_w[1] -= camera_pos[4]; temp_point_w[2] -= camera_pos[5];
    temp_point_w[0] -= camera_pos[4]; temp_point_w[1] -= camera_pos[5]; temp_point_w[2] -= camera_pos[6];
    ceres::QuaternionRotatePoint(camera_pos, temp_point_w, temp_point_c);
    point_image_frame[0] = temp_point_c[0];
    point_image_frame[1] = -temp_point_c[2]; // RH system of ROS to RH system of Camera
    point_image_frame[2] = temp_point_c[1];*/
  
    double sx, sy, sz, dxi, dyi;
    double rho = this->World2Sphere(in_cloud->points[i].x, in_cloud->points[i].y, in_cloud->points[i].z,
                       camera_pos, sx, sy, sz);
    int xi, yi;
    this->Sphere2Image(sx, sy, sz, dxi, dyi);
    xi = std::round(dxi); yi = std::round(dyi);    
    if(xi >= 0 && xi < width && yi >=0 && yi < height){
      if( ((*out_ba_points)[yi][xi].rho > rho || (*out_ba_points)[yi][xi].rho == 0)  ){
        (*out_ba_points)[yi][xi].x = in_cloud->points[i].x;
        (*out_ba_points)[yi][xi].y = in_cloud->points[i].y;
        (*out_ba_points)[yi][xi].z = in_cloud->points[i].z;
        (*out_ba_points)[yi][xi].rho = rho;
        (*out_ba_points)[yi][xi].r = in_cloud->points[i].r;
        (*out_ba_points)[yi][xi].g = in_cloud->points[i].g;
        (*out_ba_points)[yi][xi].b = in_cloud->points[i].b;
//         double log_rho = max_distance * (1 - std::exp(-alpha*rho));
//         (*out_ba_points)[yi][xi].r = (int)(255.0 - std::min(log_rho*255.0/max_distance, 255.0));
//         (*out_ba_points)[yi][xi].g = (int)(255.0 - std::min(log_rho*255.0/max_distance, 255.0));
//         (*out_ba_points)[yi][xi].b = (int)(255.0 - std::min(log_rho*255.0/max_distance, 255.0));
      }
    }
  }
}

void ImLoc::DetectAKAZEfeatures(cv::Mat & in_img, std::vector<cv::KeyPoint> & out_kps){
  AKAZEOptions options;
  cv::Mat img_32;

  // Convert the images to float
  in_img.convertTo(img_32, CV_32F, 1.0/255.0, 0);

  // Create the first AKAZE object
  options.img_width = in_img.cols;
  options.img_height = in_img.rows;
  libAKAZE::AKAZE evolution(options);

  evolution.Create_Nonlinear_Scale_Space(img_32);
  evolution.Feature_Detection(out_kps);
}

void ImLoc::ComputeAKAZEdescriptors(cv::Mat & in_img, std::vector<cv::KeyPoint> & in_kps, int desc_id, cv::Mat & out_desc){
  AKAZEOptions options;
  cv::Mat img_32;

  // Convert the images to float
  in_img.convertTo(img_32, CV_32F, 1.0/255.0, 0);

  // Create the first AKAZE object
  options.img_width = in_img.cols;
  options.img_height = in_img.rows;
  options.descriptor = DESCRIPTOR_TYPE(desc_id);
  libAKAZE::AKAZE evolution(options);

  evolution.Create_Nonlinear_Scale_Space(img_32);
  evolution.Compute_Descriptors(in_kps, out_desc);
}

void ImLoc::ComputeSPHORBdescriptors(int num_desc, cv::Mat &in_img, std::vector<cv::KeyPoint> & out_kps, cv::Mat & out_desc){
  out_kps.clear();
  out_desc = Mat();
  SPHORB sorb(num_desc);
  sorb(in_img, Mat(), out_kps, out_desc);
}

void ImLoc::MatchAKAZEfeatures(cv::Mat in_desc_1, cv::Mat in_desc_2, int num_nns, std::vector<cv::DMatch> & out_matches){
  out_matches.clear();
  std::vector<std::vector<cv::DMatch> > dmatches;
  cv::Ptr<cv::DescriptorMatcher> matcher_l2 = cv::DescriptorMatcher::create("BruteForce");
  cv::Ptr<cv::DescriptorMatcher> matcher_l1 = cv::DescriptorMatcher::create("BruteForce-Hamming");
  matcher_l1->knnMatch(in_desc_1, in_desc_2, dmatches, num_nns);
  for(int i=0; i<dmatches.size(); i++){
    for(int j=0; j<dmatches[i].size(); j++){
      out_matches.push_back(dmatches[i][j]);
    }
  }
}

void ImLoc::MatchSPHORBfeatures(cv::Mat in_desc_1, cv::Mat in_desc_2, int num_nns, std::vector<cv::DMatch> & out_matches){
  out_matches.clear();
  float ratio = 0.85f;
  cv::BFMatcher matcher(cv::NORM_HAMMING, false);
  std::vector<std::vector<cv::DMatch> > dmatches;
  matcher.knnMatch(in_desc_1, in_desc_2, dmatches, num_nns);
  
  //this->_ratioTest(dmatches, ratio, out_matches);
  
  for(int i=0; i<dmatches.size(); i++){
    for(int j=0; j<dmatches[i].size(); j++){
      out_matches.push_back(dmatches[i][j]);
    }
  }
}

void ImLoc::ScaleKeypoints(std::vector<cv::KeyPoint> & in_kps, std::vector<cv::KeyPoint> & out_kps, double scale_factor){
  std::vector<cv::KeyPoint> sc_kps;
  for(int i=0; i<in_kps.size(); i++){
    sc_kps.push_back(in_kps[i]);
    sc_kps[i].pt.x = in_kps[i].pt.x * scale_factor;
    sc_kps[i].pt.y = in_kps[i].pt.y * scale_factor;
    sc_kps[i].size = in_kps[i].size * scale_factor;
  }
  out_kps = sc_kps;
}

void ImLoc::WriteBAimage(std::string fileName, std::vector<std::vector<ba_point> >*& in_ba_points, double *camera_pos){
  Json::Value img;
  img["width"] = (Json::Value::Int)(*in_ba_points)[0].size();
  img["height"] = (Json::Value::Int)(*in_ba_points).size();
  for(int i=0; i<7; i++){
    img["camera"].append(camera_pos[i]);
  }
  
   for(int i=0; i<in_ba_points->size(); i++){
    for(int j=0; j<(*in_ba_points)[i].size(); j++){
      Json::Value obj;
      if((*in_ba_points)[i][j].rho <= 0.0) continue;
      obj["x"] = (*in_ba_points)[i][j].x;
      obj["y"] = (*in_ba_points)[i][j].y;
      obj["z"] = (*in_ba_points)[i][j].z;
      obj["ba"] = (*in_ba_points)[i][j].ba;
      obj["r"] = (*in_ba_points)[i][j].r;
      obj["g"] = (*in_ba_points)[i][j].g;
      obj["b"] = (*in_ba_points)[i][j].b;
      obj["rho"] = (*in_ba_points)[i][j].rho;
      obj["xi"] = j;
      obj["yi"] = i;
      img["pixels"].append(obj);
    }
  }
  
  Json::FastWriter fast;
  std::string sFast = fast.write(img);
  std::ofstream imgfile;
  imgfile.open(fileName.c_str());
  imgfile << sFast;
  imgfile.close();
}

void ImLoc::ReadBAimage(std::string fileName, std::vector<std::vector<ba_point> >*& out_ba_points, double*& out_camera_pos){
  std::ifstream ifs(fileName.c_str());
  Json::Value img;
  ifs >> img;
  int height = img["height"].asInt();
  int width = img["width"].asInt();
  
  if (NULL != out_ba_points) 
  {
      delete out_ba_points;
  }
  out_ba_points = new std::vector<std::vector<ba_point> > (height, std::vector<ba_point>(width));
  for(int i=0; i<img["pixels"].size(); i++){
    int xi = img["pixels"][i]["xi"].asInt();
    int yi = img["pixels"][i]["yi"].asInt();
    (*out_ba_points)[yi][xi].x = img["pixels"][i]["x"].asFloat();
    (*out_ba_points)[yi][xi].y = img["pixels"][i]["y"].asFloat();
    (*out_ba_points)[yi][xi].z = img["pixels"][i]["z"].asFloat();
    (*out_ba_points)[yi][xi].ba = img["pixels"][i]["ba"].asFloat();
    (*out_ba_points)[yi][xi].r = img["pixels"][i]["r"].asUInt();
    (*out_ba_points)[yi][xi].g = img["pixels"][i]["g"].asUInt();
    (*out_ba_points)[yi][xi].b = img["pixels"][i]["b"].asUInt();
    (*out_ba_points)[yi][xi].rho = img["pixels"][i]["rho"].asFloat();
  }
  
  out_camera_pos = new double[7]();
  
  for(int i=0; i<7; i++){
    out_camera_pos[i] = img["camera"][i].asDouble();
    std::cout << out_camera_pos[i] << " ";
  }
  std::cout << "camera pose\n";
}

void ImLoc::ConvertToImage(std::vector<std::vector<ba_point> >*& in_ba_points, cv::Mat & out_image){
  int height = (*in_ba_points).size();
  int width = 0;
  if(height > 0) width = (*in_ba_points)[0].size();
  out_image = cv::Mat(height, width, CV_8UC3, cv::Scalar::all(255));
  //out_image = cv::Mat(height, width, CV_8UC3, cv::Scalar::all(0));
  for(int i = 0; i < (*in_ba_points).size(); i++){
    for(int j = 0; j < (*in_ba_points)[i].size(); j++){
      if((*in_ba_points)[i][j].rho != 0) {
        out_image.at<cv::Vec3b>(i, j)[0] = (*in_ba_points)[i][j].b;
        out_image.at<cv::Vec3b>(i, j)[1] = (*in_ba_points)[i][j].g;
        out_image.at<cv::Vec3b>(i, j)[2] = (*in_ba_points)[i][j].r;
//         out_image.at<cv::Vec3b>(i, j)[0] = 255;
//         out_image.at<cv::Vec3b>(i, j)[1] = 255;
//         out_image.at<cv::Vec3b>(i, j)[2] = 255;
      }
    }
  }
}

bool ImLoc::IsWithinImageBounds(int x, int y, int width, int height){
  return (x >= 0 && x < width && y > 0 && y < height);
}

void ImLoc::Get3dPoints(std::vector<cv::KeyPoint> & in_kps, std::vector<std::vector<ba_point> >*& in_ba_points,
                        std::vector<cv::Vec4f> & in_kps3d, float std_threshold){
  in_kps3d.clear();
  int num_met0 = 0, num_met1 = 0;
  int height = (*in_ba_points).size();
  int width = 0;
  if(height > 0) width = (*in_ba_points)[0].size();
  for(int i=0; i<in_kps.size(); i++){
    int xi = in_kps[i].pt.x;
    int yi = in_kps[i].pt.y;
    float pt_x, pt_y, pt_z;
    int method = -1; // Default value for no 3D data
    if((*in_ba_points)[yi][xi].rho != 0){ // We have a valid 3D point here
      pt_x = (*in_ba_points)[yi][xi].x;
      pt_y = (*in_ba_points)[yi][xi].y;
      pt_z = (*in_ba_points)[yi][xi].z;
      
      int radius = std::min(int(in_kps[i].size/2), 10);
      radius = std::max(radius, 2);
      std::vector < cv::Vec3f > points;
      float varx, vary, varz, stdx, stdy, stdz;
      for(int j=-radius; j<radius; j++){
        for(int k=-radius; k<radius; k++){
          if (std::sqrt( j*j + k*k ) < radius && 
              this->IsWithinImageBounds(xi+j, yi+k, width, height) && 
              (*in_ba_points)[yi+k][xi+j].rho != 0){
            points.push_back(cv::Vec3f((*in_ba_points)[yi+k][xi+j].x, (*in_ba_points)[yi+k][xi+j].y, (*in_ba_points)[yi+k][xi+j].z));
          }
        }
      }
      this->ComputeVariance(points, varx, vary, varz);        
      stdx = std::sqrt(varx);
      stdy = std::sqrt(vary);
      stdz = std::sqrt(varz);
      
      if(stdx <= std_threshold && stdy <= std_threshold && stdz <= std_threshold){
        method = 0; // 3D data calculated from single point
        num_met0++;
      }
    }
    else{ // Get 3D point from the mean in the radius
      int radius = std::min(int(in_kps[i].size/2), 10);
      radius = std::max(radius, 2);
      int counter = 0;
      pt_x = 0.0; pt_y = 0.0; pt_z = 0.0;
      std::vector < cv::Vec3f > points;
      for(int j=-radius; j<radius; j++){
        for(int k=-radius; k<radius; k++){
          if (std::sqrt( j*j + k*k ) < radius && 
              this->IsWithinImageBounds(xi+j, yi+k, width, height) && 
              (*in_ba_points)[yi+k][xi+j].rho != 0){          
            pt_x = pt_x + (*in_ba_points)[yi+k][xi+j].x;
            pt_y = pt_y + (*in_ba_points)[yi+k][xi+j].y;
            pt_z = pt_z + (*in_ba_points)[yi+k][xi+j].z;
            points.push_back(cv::Vec3f((*in_ba_points)[yi+k][xi+j].x, (*in_ba_points)[yi+k][xi+j].y, (*in_ba_points)[yi+k][xi+j].z));
            counter++;
          }
        }
      }
      float varx, vary, varz, stdx, stdy, stdz;
      if(counter != 0){
        pt_x = pt_x / (float)counter;
        pt_y = pt_y / (float)counter;
        pt_z = pt_z / (float)counter;
        this->ComputeVariance(points, varx, vary, varz);        
        stdx = std::sqrt(varx);
        stdy = std::sqrt(vary);
        stdz = std::sqrt(varz);
        //std::cout << stdx << " " << stdy << " " << stdz <<"\n";
      }
      
      //std::cout << radius << "radius " << pt_x << " " << pt_y << " " << pt_z << " " << counter <<"\n";
      if(counter==0 || 
         (pt_x == 0 && pt_y == 0 && pt_z == 0) || 
         (stdx > std_threshold || stdy > std_threshold || stdz > std_threshold)){
        method = -2; // Still invalid data
      }
      else{
        method = 1; // 3D data calculated from neibour avg
        num_met1++;
      }
    }
    
    // Time to add the data to vector
    in_kps3d.push_back(cv::Vec4f(pt_x, pt_y, pt_z, method));
    //if(method < 0)
    //  std::cout << "Err! " << method << " method\n"; 
  }
  //std::cout << "Got " << num_met0 << " method 0 and " << num_met1 << " method 1 3D points\n"; 
}

void ImLoc::ComputeDeviationHeatmap(vector< vector< ba_point > >*& in_ba_points, int radius, float clip_std, Mat& out_heatmap)
{
  int height = (*in_ba_points).size();
  int width = 0;
  if(height > 0) width = (*in_ba_points)[0].size();
  cv::Mat grey_heatmap(height, width, CV_8UC1,cv::Scalar(0));
  for(int xi=0; xi<width; xi++){
    for(int yi=0; yi<height; yi++){
      float pt_x, pt_y, pt_z;
      std::vector < cv::Vec3f > points;
      float varx, vary, varz, stdx, stdy, stdz;
      for(int j=-radius; j<radius; j++){
        for(int k=-radius; k<radius; k++){
          if (std::sqrt( j*j + k*k ) < radius && 
              this->IsWithinImageBounds(xi+j, yi+k, width, height) && 
              (*in_ba_points)[yi+k][xi+j].rho != 0){
            points.push_back(cv::Vec3f((*in_ba_points)[yi+k][xi+j].x, (*in_ba_points)[yi+k][xi+j].y, (*in_ba_points)[yi+k][xi+j].z));
          }
        }
      }
    
      this->ComputeVariance(points, varx, vary, varz);        
      stdx = std::sqrt(varx);
      stdy = std::sqrt(vary);
      stdz = std::sqrt(varz);
      float min_std = std::min(stdx, std::min(stdy,stdz));
      if (min_std > clip_std) min_std = clip_std;
      if(points.size() > 0){
        grey_heatmap.at<uchar>(yi,xi) = std::round(min_std*255.0/clip_std);
      }
    }
  }
  cv::applyColorMap(grey_heatmap, out_heatmap, COLORMAP_JET);
  //imshow("grey",grey_heatmap);
  //imshow("heat",out_heatmap);
  //waitKey(0);
}


std::string ImLoc::type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

void ImLoc::WriteDesc3DPoints(string fileName, std::vector<cv::KeyPoint> & in_kps, 
            std::vector<cv::Vec4f> & in_kps3d, cv::Mat & in_desc){
  Json::Value descPoints;
  for(int i=0; i < in_kps.size(); i++){
    Json::Value obj;
    obj["k"].append(int(in_kps[i].pt.x));
    obj["k"].append(int(in_kps[i].pt.y));
    obj["k"].append(int(in_kps[i].size));
    
    for(int j=0; j<4; j++){
      obj["3"].append(float(in_kps3d[i][j]));
    }
    
    for(int j=0; j<in_desc.cols; j++){
      obj["d"].append(uint(in_desc.at<uchar>(i, j)));
    }
    descPoints["objects"].append(obj);
  }
  Json::FastWriter fast;
  std::string sFast = fast.write(descPoints);
  std::ofstream imgfile;
  imgfile.open(fileName.c_str());
  imgfile << sFast;
  imgfile.close();
}

void ImLoc::ReadDesc3DPoints(string fileName, std::vector<cv::KeyPoint> & out_kps, 
           std::vector<cv::Vec4f> & out_kps3d, cv::Mat & out_desc){
  out_kps.clear();
  out_kps3d.clear();
  
  std::ifstream ifs(fileName.c_str());
  Json::Value descPoints;
  ifs >> descPoints;
  
  int num_objs = descPoints["objects"].size();  
  if (num_objs <= 0) return;
  
  int desc_len = descPoints["objects"][0]["d"].size();
  out_desc = cv::Mat(num_objs, desc_len, CV_8UC1);
  
  for(int i=0; i<num_objs; i++){
    for(int j=0; j<desc_len; j++)
      out_desc.at<uchar>(i, j) = descPoints["objects"][i]["d"][j].asUInt();
    out_kps3d.push_back(cv::Vec4f(descPoints["objects"][i]["3"][0].asFloat(),
          descPoints["objects"][i]["3"][1].asFloat(),
          descPoints["objects"][i]["3"][2].asFloat(),
          descPoints["objects"][i]["3"][3].asFloat() ));
    cv::KeyPoint kp;
    kp.pt.x = descPoints["objects"][i]["k"][0].asInt();
    kp.pt.y = descPoints["objects"][i]["k"][1].asInt();
    kp.size = descPoints["objects"][i]["k"][2].asInt();
    out_kps.push_back(kp);
  }
}

void ImLoc::Image2Sphere(int x, int y, double & sx, double & sy, double & sz){
  double mod_obs_x = x;
  if(x >= _image_center*2)
    mod_obs_x = _image_height*2 - x - 1;
  double phi = ((double)(y - _image_center))/_scale_factor;
  double theta = ((double)(mod_obs_x - _image_center))/_scale_factor;
  double obs_ps[3] = {0.0};
  obs_ps[0] = std::sin(theta) * std::cos(phi);
  obs_ps[1] = std::sin(phi);
  obs_ps[2] = std::cos(theta) * std::cos(phi);
  if((x >= _image_center*2 && obs_ps[2] >= 0) || // Point on right side should have neg z
      (x < _image_center*2 && obs_ps[2] < 0)){ // Point on left side should have pos z
    obs_ps[2] = -obs_ps[2];
  }
  double rho = std::sqrt((obs_ps[0]*obs_ps[0])+
                         (obs_ps[1]*obs_ps[1])+
                         (obs_ps[2]*obs_ps[2]));
  // 3D points projected on the sphere
  sx = obs_ps[0]/rho;
  sy = obs_ps[1]/rho;
  sz = obs_ps[2]/rho;
}

void ImLoc::Sphere2Image(double sx, double sy, double sz, double& x, double& y){
  double phi = std::asin(sy);
  double theta = std::asin(sx / std::cos(phi));
  double xi = (theta * _scale_factor) + _image_center;
  double yi = (phi * _scale_factor) + _image_center;
  
  if (std::isnan(xi))
    xi = 0;
  y = yi;
  if(sz >= 0)
    x = xi;
  else
    x = _image_height*2 - 1 - xi;
}

double ImLoc::World2Sphere(double x, double y, double z, double * camera, double & sx, double & sy, double & sz){
  double point_camera[3] = {0.0}; double temp_point_c1[3] = {0.0};
  double point_image_frame[3] = {0.0};
  double point_w[3] = {x,y,z};
  point_w[0] -= camera[4]; point_w[1] -= camera[5]; point_w[2] -= camera[6];
  ceres::QuaternionRotatePoint(camera, point_w, point_camera);

  point_image_frame[0] = point_camera[0];
  point_image_frame[1] = -point_camera[2]; // RH system of ROS to RH system of Camera
  point_image_frame[2] = point_camera[1];

  double rho = sqrt(point_image_frame[0]*point_image_frame[0] +
        point_image_frame[1]*point_image_frame[1] +
        point_image_frame[2]*point_image_frame[2]);
  sx = point_image_frame[0] / rho;
  sy = point_image_frame[1] / rho;
  sz = point_image_frame[2] / rho;
  return rho;
}

void ImLoc::RunOptimizer(int num_pairs, double s_obs[][3], double world_obs[][3], double * orientation, double angle_bound, double pos_bound, double * out_cam, bool use_trans_initial){
  double tx = 0, ty = 0, tz = 0, rx  = 0, ry = 0, rz = 0;
  if (use_trans_initial){
    tx = out_cam[4]; ty = out_cam[5]; tz = out_cam[6];
  }  
  double rot[3] = {0};
  ceres::QuaternionToAngleAxis(orientation, rot);
  rx = rot[0]; ry = rot[1]; rz = rot[2];
  if (use_trans_initial) std::cout << "Rotations: " << rx << " " << ry << " " << rz << std::endl;
  ceres::Problem problem;
  for (int j = 0; j < num_pairs; j++) {
    problem.AddResidualBlock(
      new ceres::AutoDiffCostFunction<SphericalReprojectionError, 3,1,1,1,1,1,1>(
  new SphericalReprojectionError(s_obs[j][0],
          s_obs[j][1],
          s_obs[j][2],
          world_obs[j][0],
          world_obs[j][1],
          world_obs[j][2],
          orientation[0],
          orientation[1],
          orientation[2],
          orientation[3])
      ), NULL,
      &tx, &ty, &tz, &rx, &ry, &rz
    );
    if (use_trans_initial) 
      std::cout << "Giving: " << s_obs[j][0] << " " << s_obs[j][1] << " " << s_obs[j][2] << " " << world_obs[j][0] << " " << world_obs[j][1] << " " << world_obs[j][2] << std::endl;
  }
  problem.SetParameterLowerBound(&rx, 0, rx-angle_bound);
  problem.SetParameterLowerBound(&ry, 0, ry-angle_bound);
  problem.SetParameterLowerBound(&rz, 0, rz-angle_bound);
  problem.SetParameterUpperBound(&rx, 0, rx+angle_bound);
  problem.SetParameterUpperBound(&ry, 0, ry+angle_bound);
  problem.SetParameterUpperBound(&rz, 0, rz+angle_bound);
  problem.SetParameterLowerBound(&tx, 0, tx-pos_bound);
  problem.SetParameterLowerBound(&ty, 0, ty-pos_bound);
  problem.SetParameterLowerBound(&tz, 0, tz-pos_bound);
  problem.SetParameterUpperBound(&tx, 0, tx+pos_bound);
  problem.SetParameterUpperBound(&ty, 0, ty+pos_bound);
  problem.SetParameterUpperBound(&tz, 0, tz+pos_bound);
  
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_SCHUR;
  if (use_trans_initial) options.minimizer_progress_to_stdout = true;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  if (use_trans_initial) std::cout << summary.FullReport() << "\n";
  
  rot[0] = rx; rot[1] = ry; rot[2] = rz;
  if (use_trans_initial) std::cout << rx << ", " << ry << ", " << rz << " angles\n";
  double quat[4];
  ceres::AngleAxisToQuaternion(rot, quat);
  double solved_camera[7] = {quat[0], quat[1], quat[2], quat[3], tx, ty, tz}; // r, p, y, x, y, z
  std::copy(solved_camera, solved_camera+7, out_cam);
}

void ImLoc::FindInliers(std::vector<cv::DMatch> & in_matches, std::vector<cv::Vec4f> & in_kps3d, int height,
      std::vector<cv::KeyPoint> & in_kps, double * solved_camera, double threshold,
      int & out_num_inliers, std::vector<int> & out_in_ids){
  out_num_inliers = 0;
  for(int j=0; j<in_matches.size(); j++){
    if(in_kps3d[in_matches[j].trainIdx][3] < 0) continue;
    double q_sx, q_sy, q_sz, db_sx, db_sy, db_sz;
    this->Image2Sphere(in_kps[in_matches[j].queryIdx].pt.x, in_kps[in_matches[j].queryIdx].pt.y,
      q_sx, q_sy, q_sz);
    this->World2Sphere(in_kps3d[in_matches[j].trainIdx][0], in_kps3d[in_matches[j].trainIdx][1],
      in_kps3d[in_matches[j].trainIdx][2], solved_camera, db_sx, db_sy, db_sz);
    double distance = std::sqrt( (q_sx-db_sx)*(q_sx-db_sx) +
                                 (q_sy-db_sy)*(q_sy-db_sy) +
                                 (q_sz-db_sz)*(q_sz-db_sz));
    if(distance < threshold){ // Inlier
      out_in_ids.push_back(j);
      out_num_inliers++;
    }
  }
}

void ImLoc::SolveSnPRansac(std::vector<cv::DMatch> & in_matches, std::vector<cv::Vec4f> & in_kps3d, std::vector<cv::KeyPoint> & in_kps,
         int num_ransac, double threshold, int height, double * orientation, double angle_bound, double pos_bound, double * final_trans, std::vector<int> & out_inliers, double * GT){
  std::srand((unsigned)std::time(NULL));
  int max_inliers = 0;
  std::vector<int> final_in_ids;
  int snp_points = 6;
  std::vector< cv::Vec3f > clust_points;
  out_inliers.clear();
  
//   int threads = omp_get_num_threads();
//   std::cout << threads << " num threads\n";
  #pragma omp parallel for num_threads(7) shared(final_in_ids, max_inliers, clust_points)
  for(int i = 0; i < num_ransac; i++){
    double (*world_obs)[3] = new double[snp_points][3];
    double (*s_obs)[3] = new double[snp_points][3];
    double solved_camera[7];
    
    //Take snp_points random match pairs
    int pt_c = 0;
    while(pt_c < snp_points){
      int id = rand() % in_matches.size();
      if(in_kps3d[in_matches[id].trainIdx][3] < 0) continue;
      this->Image2Sphere(in_kps[in_matches[id].queryIdx].pt.x,
                         in_kps[in_matches[id].queryIdx].pt.y,
                         s_obs[pt_c][0], s_obs[pt_c][1], s_obs[pt_c][2]);
      world_obs[pt_c][0] = in_kps3d[in_matches[id].trainIdx][0];
      world_obs[pt_c][1] = in_kps3d[in_matches[id].trainIdx][1];
      world_obs[pt_c][2] = in_kps3d[in_matches[id].trainIdx][2];
      pt_c++;
    }
    
    this->RunOptimizer(snp_points, s_obs, world_obs, orientation, angle_bound, pos_bound, solved_camera);
    
    int num_inliers;
    std::vector<int> run_in_ids;    
    this->FindInliers(in_matches, in_kps3d, height, in_kps, solved_camera, threshold, num_inliers, run_in_ids);
    
    if(num_inliers > max_inliers){
      max_inliers = num_inliers;
      final_in_ids = run_in_ids;
      std::copy(solved_camera, solved_camera+7, final_trans);
//       std::cout << "Interim num-inliers: " << num_inliers << " in " << i << std::endl;
//       for(int i=0; i<7; i++){
//         std::cout << solved_camera[i] << " ";
//       }
//       std::cout << " Interim translation" << std::endl;
//       double rot[3];
//       ceres::QuaternionToAngleAxis(solved_camera, rot);
//       std::cout << rot[0]*180.0/M_PI << "," << rot[1]*180.0/M_PI << "," << rot[2]*180.0/M_PI << std::endl;
//       if(GT != nullptr){
//         if( std::fabs(solved_camera[4]-GT[0]) < 2.0 && 
//             std::fabs(solved_camera[5]-GT[1]) < 2.0 && 
//             std::fabs(solved_camera[6]-GT[2]) < 2.0){
//           clust_points.push_back(cv::Vec3f(solved_camera[4], solved_camera[5], solved_camera[6]));
//         }
//       }
    }
    delete [] world_obs;
    delete [] s_obs;
  }
  
  for(int i=0; i<7; i++){
    std::cout << final_trans[i] << " ";
  }
  std::cout << " Solved Camera with inliers " << max_inliers << std::endl;
  out_inliers = final_in_ids;
  
//   float varx, vary, varz, stdx, stdy, stdz;
//   if(clust_points.size() > 0){
//     this->ComputeVariance(clust_points, varx, vary, varz);        
//     stdx = std::sqrt(varx);
//     stdy = std::sqrt(vary);
//     stdz = std::sqrt(varz);
//     std::cout << stdx << ", " << stdy << ", " << stdz << " Standard deviation in estimation" << std::endl;
//   }
  
//   std::cout << "Calculating final position based on inliers\n";
//   // Get all the inliers to solve for the final camera position
//   // Setup dynamic arrays
//   double (*world_obs)[3] = new double[max_inliers][3];
//   double (*s_obs)[3] = new double[max_inliers][3];
//   int pt_c = 0;
//   for(int i=0; i<max_inliers; i++){        
//     if(in_kps3d[in_matches[final_in_ids[i]].trainIdx][3] != 0) continue;
//       this->Image2Sphere(in_kps[in_matches[final_in_ids[i]].queryIdx].pt.x,
//                          in_kps[in_matches[final_in_ids[i]].queryIdx].pt.y,
//                          s_obs[pt_c][0], s_obs[pt_c][1], s_obs[pt_c][2]);
//       world_obs[pt_c][0] = in_kps3d[in_matches[final_in_ids[i]].trainIdx][0];
//       world_obs[pt_c][1] = in_kps3d[in_matches[final_in_ids[i]].trainIdx][1];
//       world_obs[pt_c][2] = in_kps3d[in_matches[final_in_ids[i]].trainIdx][2];
//       
//       double t_x, t_y, t_z;
//       this->World2Sphere(world_obs[pt_c][0], world_obs[pt_c][1], world_obs[pt_c][2],
//        final_camera, t_x, t_y, t_z);
// //       std::cout << "Comparision bw s_obs, transformed world_obs, and world_obs:\n" 
// //    << s_obs[pt_c][0] << " " << t_x << " " << world_obs[pt_c][0] << "\n"
// //    << s_obs[pt_c][1] << " " << t_y << " " << world_obs[pt_c][1] << "\n"
// //    << s_obs[pt_c][2] << " " << t_z << " " << world_obs[pt_c][2] << "\n";
//       pt_c++;    
//   }
//   this->RunOptimizer(pt_c, s_obs, world_obs, final_camera, true);
//   this->FindInliers(in_matches, in_kps3d, height, in_kps, final_camera, threshold, max_inliers, final_in_ids);
//   
//   delete [] world_obs;
//   delete [] s_obs;
  
}

void ImLoc::DrawMatches(const cv::Mat& img1, const std::vector<cv::KeyPoint>& keypoints1,
      const cv::Mat& img2, const std::vector<cv::KeyPoint>& keypoints2,
      const std::vector<cv::DMatch>& matches1to2, cv::Mat& outImg, const cv::Scalar& matchColor,
      const cv::Scalar& singlePointColor, const std::vector<char>& matchesMask, int flags , bool vertical){
  if(!vertical)
    cv::drawMatches(img1, keypoints1, img2, keypoints2, matches1to2, outImg, matchColor, singlePointColor, matchesMask, flags);
  else
  {
    if( !matchesMask.empty() && matchesMask.size() != matches1to2.size() )
      CV_Error( CV_StsBadSize, "matchesMask must have the same size as matches1to2" );

    cv::Mat outImg1, outImg2;
    this->_prepareImgAndDrawKeypoints( img1, keypoints1, img2, keypoints2,
      outImg, outImg1, outImg2, singlePointColor, flags );

    // draw matches
    for( size_t m = 0; m < matches1to2.size(); m++ )
    {
      int i1 = matches1to2[m].queryIdx;
      int i2 = matches1to2[m].trainIdx;
      if( matchesMask.empty() || matchesMask[m] )
      {
        const cv::KeyPoint &kp1 = keypoints1[i1], &kp2 = keypoints2[i2];
        this->_drawMatch( outImg, outImg1, outImg2, kp1, kp2, matchColor, flags );
      }
    }
  }
}

void ImLoc::DrawMultiMatches(std::vector<cv::DMatch> & in_matches, std::vector<cv::KeyPoint> & q_kps, std::vector<cv::KeyPoint> & db_kps,
         std::vector<int> & in_end_ids, std::vector<cv::Mat> & in_db_images, cv::Mat & in_q_image,
         std::vector<int> & inliers, bool draw_inliers){
  cv::namedWindow("Display Window", CV_WINDOW_NORMAL);
  for(int i=0; i<in_db_images.size(); i++){
    cv::Mat imgMatches;
    std::vector<cv::DMatch> this_pair_matches;
    
    if(draw_inliers){
      for(int j=0; j<inliers.size(); j++){
  int start_id = (i==0)?0:in_end_ids[i-1];
  int end_id = in_end_ids[i];
  if(in_matches[inliers[j]].trainIdx < start_id || in_matches[inliers[j]].trainIdx >= end_id) continue;
  this_pair_matches.push_back(in_matches[inliers[j]]);
      }
    }
    else{   
      for(int j=0; j<in_matches.size(); j++){
  int start_id = (i==0)?0:in_end_ids[i-1];
  int end_id = in_end_ids[i];
  if(in_matches[j].trainIdx < start_id || in_matches[j].trainIdx >= end_id) continue;
  this_pair_matches.push_back(in_matches[j]);
      }
    }
    this->DrawMatches(in_q_image, q_kps, in_db_images[i], db_kps, this_pair_matches, imgMatches, cv::Scalar::all(-1), cv::Scalar::all(-1),  
          std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS,true);
    if(draw_inliers){
      std::stringstream name;
      name << "in_matches" << i << ".jpg";
      cv::imwrite(name.str(), imgMatches);
    }
    cv::imshow("Display Window", imgMatches);
    cv::waitKey(0);
  }
  cv::destroyWindow("Display Window");
}

void ImLoc::ComputeVariance(std::vector< cv::Vec3f >& points, float &out_varx, float &out_vary, float &out_varz)
{
  out_varx = 0.0; out_vary = 0.0; out_varz = 0.0;
  float meanx=0; float meany=0; float meanz=0;
  for(int i=0; i<points.size(); i++){
    meanx += points[i][0];
    meany += points[i][1];
    meanz += points[i][2];
  }
  meanx = meanx / points.size();
  meany = meany / points.size();
  meanz = meanz / points.size();
  for(int i=0; i<points.size(); i++){
    out_varx += ((points[i][0] - meanx)*(points[i][0] - meanx));
    out_vary += ((points[i][1] - meany)*(points[i][1] - meany));
    out_varz += ((points[i][2] - meanz)*(points[i][2] - meanz));
  }
  out_varx = out_varx / points.size();
  out_vary = out_vary / points.size();
  out_varz = out_varz / points.size();
}


void ImLoc::_drawKeypoint( cv::Mat& img, const cv::KeyPoint& p, const cv::Scalar& color, int flags ){
  const int draw_shift_bits = 4;
  const int draw_multiplier = 1 << draw_shift_bits;
  CV_Assert( !img.empty() );
  cv::Point center( cvRound(p.pt.x * draw_multiplier), cvRound(p.pt.y * draw_multiplier) );

  if( flags & cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS )
  {
    int radius = cvRound(p.size/2 * draw_multiplier); // KeyPoint::size is a diameter

    // draw the circles around keypoints with the keypoints size
    cv::circle( img, center, radius, color, 1, CV_AA, draw_shift_bits );

    // draw orientation of the keypoint, if it is applicable
    if( p.angle != -1 )
    {
      float srcAngleRad = p.angle*(float)CV_PI/180.f;
      cv::Point orient( cvRound(cos(srcAngleRad)*radius ),
        cvRound(sin(srcAngleRad)*radius )
        );
      cv::line( img, center, center+orient, color, 1, CV_AA, draw_shift_bits );
    }
#if 0
      else
      {
  // draw center with R=1
  int radius = 1 * draw_multiplier;
  circle( img, center, radius, color, 1, CV_AA, draw_shift_bits );
      }
#endif
  }
  else
  {
    // draw center with R=3
    int radius = 3 * draw_multiplier;
    cv::circle( img, center, radius, color, 1, CV_AA, draw_shift_bits );
  }
}

void ImLoc::_ratioTest(const std::vector<std::vector<cv::DMatch> >& knMatches, float maxRatio, std::vector<cv::DMatch>& goodMatches){
  goodMatches.clear();
  for (size_t i=0; i< knMatches.size(); i++){
    const cv::DMatch& best = knMatches[i][0];
    const cv::DMatch& good = knMatches[i][1];
    assert(best.distance <= good.distance);
    float ratio = (best.distance / good.distance);
    if (ratio <= maxRatio){
      goodMatches.push_back(best);
    }
  }
}

void ImLoc::_prepareImgAndDrawKeypoints(const cv::Mat& img1, const std::vector<cv::KeyPoint>& keypoints1,
                                         const cv::Mat& img2, const std::vector<cv::KeyPoint>& keypoints2,
                                         cv::Mat& outImg, cv::Mat& outImg1, cv::Mat& outImg2,
                                         const cv::Scalar& singlePointColor, int flags){
  cv::Size size( MAX(img1.cols, img2.cols), img1.rows + img2.rows);
  if( flags & DrawMatchesFlags::DRAW_OVER_OUTIMG )
  {
    if( size.width > outImg.cols || size.height > outImg.rows )
      CV_Error( CV_StsBadSize, "outImg has size less than need to draw img1 and img2 together" );
    outImg1 = outImg( Rect(0, 0, img1.cols, img1.rows) );
    outImg2 = outImg( Rect(0, img1.rows, img2.cols, img2.rows) );
  }
  else
  {
    outImg.create( size, CV_MAKETYPE(img1.depth(), 3) );
    outImg = cv::Scalar::all(0);
    outImg1 = outImg( Rect(0, 0, img1.cols, img1.rows) );
    outImg2 = outImg( Rect(0, img1.rows, img2.cols, img2.rows) );

    if( img1.type() == CV_8U )
      cvtColor( img1, outImg1, CV_GRAY2BGR );
    else
      img1.copyTo( outImg1 );

    if( img2.type() == CV_8U )
      cvtColor( img2, outImg2, CV_GRAY2BGR );
    else
      img2.copyTo( outImg2 );
  }

  // draw keypoints
  if( !(flags & DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS) )
  {
    Mat _outImg1 = outImg( Rect(0, 0, img1.cols, img1.rows) );
    drawKeypoints( _outImg1, keypoints1, _outImg1, singlePointColor, flags + DrawMatchesFlags::DRAW_OVER_OUTIMG );

    Mat _outImg2 = outImg( Rect(0, img1.rows, img2.cols, img2.rows) );
    drawKeypoints( _outImg2, keypoints2, _outImg2, singlePointColor, flags + DrawMatchesFlags::DRAW_OVER_OUTIMG );
  }
}

void ImLoc::_drawMatch( cv::Mat& outImg, cv::Mat& outImg1, cv::Mat& outImg2 ,
  const cv::KeyPoint& kp1, const cv::KeyPoint& kp2, const cv::Scalar& matchColor, int flags ){
  const int draw_shift_bits = 4;
  const int draw_multiplier = 1 << draw_shift_bits;
  cv::RNG& rng = theRNG();
  bool isRandMatchColor = matchColor == cv::Scalar::all(-1);
  cv::Scalar color = isRandMatchColor ? cv::Scalar( rng(256), rng(256), rng(256) ) : matchColor;

  this->_drawKeypoint( outImg1, kp1, color, flags );
  this->_drawKeypoint( outImg2, kp2, color, flags );

  cv::Point2f pt1 = kp1.pt,
    pt2 = kp2.pt,
    dpt2 = cv::Point2f(pt2.x, std::min(pt2.y+outImg1.rows, float(outImg.rows-1)));

  cv::line( outImg,
    cv::Point(cvRound(pt1.x*draw_multiplier), cvRound(pt1.y*draw_multiplier)),
    cv::Point(cvRound(dpt2.x*draw_multiplier), cvRound(dpt2.y*draw_multiplier)),
    color, 1, CV_AA, draw_shift_bits );
}
